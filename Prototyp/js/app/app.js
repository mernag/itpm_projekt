var myApp = angular.module('CircleOfClothes', ['ui.router']);
myApp.config(function($stateProvider, $urlRouterProvider) {

  //
  // For any unmatched url, redirect to /state1
  //
  // Now set up the states
  $stateProvider
    .state('main', {
      url: "/",
      templateUrl: "views/main.html"
    })
    .state('details', {
      url: "/details/:itemId",
      templateUrl: "views/details.html",
      controller: function($scope, $stateParams, ItemService, UserService){
          $scope.getItem = function(){
            var id = $stateParams.itemId;
            for(var i = 0; i < ItemService.items.length; i++)
            {
              if(ItemService.items[i].id == id)
              {
                return ItemService.items[i];
              }
            }
            return null;
          }
          $scope.item = $scope.getItem();
          $scope.user = UserService.getUser($scope.item.user);
      }
    })
    .state('search', {
      url: "/search/:search",
      templateUrl: "views/search.html",
      controller: function($scope, $stateParams, ItemService, UserService){
        $scope.userService = UserService;
        $scope.search = $stateParams.search;
        $scope.items = ItemService.items;     
      }
    })
    .state('login', {
      url: "/login",
      templateUrl: "views/login.html"
    });
    
    
  $urlRouterProvider.otherwise("/");
})
.factory("ItemService", function(){
  return {
    items: [{
        id: 1,
        image: "/img/items/a1.jpg",
        name: "Biete diesen zwei Jahre alten Anzug zur Leihung an.",
        price: "30€ / Tag",
        user: 1,
        
      }, {
        id: 2,
        image: "/img/items/a2.jpg",
        name: "Biete diesen drei Jahre alten Anzug zur Leihung an.",
        price: "50€ / Tag",
        user: 1
      }, {
        id: 3,
        image: "/img/items/k1.jpg",
        name: "Biete dieses einmal getragene Kleid zur Leihung an.",
        price: "30€ / Tag",
        user: 2
      }, {
        id: 4,
        image: "/img/items/k2.jpg",
        name: "Biete diesen zwei Jahre alte Kleid zur Leihung an.",
        price: "30€ / Tag",
        user: 2
      }, {
        id: 5,
        image: "/img/items/s1.jpg",
        name: "Biete dieses Paar Designerschuhe der Größe 42 zur Leihung an.",
        price: "30€ / Tag",
        user: 1
      }, {
        id: 6,
        image: "/img/items/s2.jpg",
        name: "Biete dieses zwei Jahre alte Paar Schuhe in Größe 47 zur Leihung an.",
        price: "30€ / Tag",
        user: 1
      }, {
        id: 7,
        image: "/img/items/t1.jpg",
        name: "Biete diese Tasche zur Leihung an.",
        price: "30€ / Tag",
        user: 2
      }, {
        id: 8,
        image: "/img/items/t2.jpg",
        name: "Biete diese Tasche zur Leihung an.",
        price: "30€ / Tag",
        user: 2
      }]
  }
})
.factory("UserService", function(){
    return {
        users: [
            {
                id: 1,
                name: "Max Müller",
                rating: "5/5",
                image: "/img/usr/male.png"
            },
            {
                id: 2, 
                name: "Heike Meier",
                rating: "4/5",
                image: "/img/usr/female.png"
            }
        ],
        getUser: function(id)
        {
            for(var i = 0; i < this.users.length; i++)
            {
                if(this.users[i].id == id)
                {
                    return this.users[i];
                }
            }
            return null;
        }
    };
}).filter('range', function() {
  return function(input, total) {
    total = parseInt(total);

    for (var i=0; i<total; i++) {
      input.push(i);
    }

    return input;
}});